import React from 'react'
import _ from 'lodash'
import logo from '../../assets/topcoder-logo.svg'
import './styles.css'

class SearchEngine extends React.PureComponent {
  constructor () {
    super()

    this.state = {
      query: ''
    }

    this.onTextChange = this.onTextChange.bind(this)
    this.onSearch = this.onSearch.bind(this)
  }

  onTextChange (e) {
    this.setState({ query: e.target.value || '' })
  }

  async onSearch () {
    await this.props.onSearch(this.state.query)
  }

  render () {
    return (
      <>
        <div className={`search-engine ${this.props.results && this.props.results.length > 0 ? 'with-results' : ''}`}>
          <img src={logo} alt="Logo" />
          <div className="search-box">
            <input
              type="text"
              placeholder="Search members..."
              value={this.state.query}
              onChange={this.onTextChange}
            />
            <button
              type="button"
              onClick={this.onSearch}
              disabled={_.trim(this.state.query) === ''}
            >
              Search
            </button>
          </div>
        </div>
        {
          this.props.results && this.props.results.length > 0 && (
            <div className="search-results">
              <h4>Found {this.props.results.length} results for the search term: {this.state.query}</h4>
              {
                _.map(this.props.results, (member) => (
                  <div className="member-profile" key={member.handle}>
                    {
                      member.photoURL ? <img src={member.photoURL} alt={member.handle} /> : <div className="img-placeholder" />
                    }
                    <div className="info">
                      <a href={`http://www.topcoder.com/members/${member.handle}`}>{member.handle}</a>
                      <div>Country: {member.homeCountryCode}</div>
                      <div>Rating: {_.get(member, 'maxRating.rating', 0)}</div>
                      <div>Skills: {_.map(member.skills, (value, key) => value.tagName).join(', ')}</div>
                    </div>
                  </div>
                ))
              }
            </div>
          )
        }
      </>
    )
  }
}

export default SearchEngine
