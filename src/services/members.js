/**
 * Member Service
 */
import axios from 'axios'
import { CONFIGS } from '../config'

/**
 * Search members using the Topcoder Member API
 * @param {String} handle the handle to search for
 * @returns {Array<Object>} the results
 */
export async function searchMembers (handle) {
  try {
    const { data } = await axios.get(CONFIGS.API_BASE_URL, {
      params: {
        handle,
        fields: 'handle,homeCountryCode,skills,maxRating,photoURL'
      }
    })
    return data
  } catch (e) {
    console.error('Failed to load members')
    console.log(e)
    return []
  }
}
