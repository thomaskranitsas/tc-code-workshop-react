import React from 'react'
import SearchEngineComponent from '../components/SearchEngine'
import { searchMembers } from '../services/members'

class SearchEngine extends React.PureComponent {
  constructor () {
    super()
    this.state = {
      data: []
    }

    this.searchMembers = this.searchMembers.bind(this)
  }

  async searchMembers (handle) {
    const data = await searchMembers(handle)
    this.setState({
      data
    })
  }

  render () {
    return (
      <SearchEngineComponent
        results={this.state.data}
        onSearch={this.searchMembers}
      />
    )
  }
}

export default SearchEngine
